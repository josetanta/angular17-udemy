import { Component } from '@angular/core';
import { PersonFilter } from '../../models/person-filter';

@Component({
  selector: 'app-search-box-timer-page',
  templateUrl: './search-box-timer-page.component.html',
  styleUrl: './search-box-timer-page.component.scss',
})
export class SearchBoxTimerPageComponent {
  filtering = new PersonFilter();
}
