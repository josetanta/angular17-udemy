import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBoxTimerPageComponent } from './search-box-timer-page.component';

describe('SearchBoxTimerPageComponent', () => {
  let component: SearchBoxTimerPageComponent;
  let fixture: ComponentFixture<SearchBoxTimerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SearchBoxTimerPageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchBoxTimerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
