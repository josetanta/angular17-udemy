export interface PersonFilter {
  firstname: string;
  lastname: string;
  dateOfBirth: Date;
}

export class PersonFilter {
  static createPersonFilter(firstname: string, lastname: string, dateOfBirth: Date) {
    let p = new PersonFilter();
    p.firstname = firstname;
    p.lastname = lastname;
    p.dateOfBirth = dateOfBirth;
    return p;
  }
}
