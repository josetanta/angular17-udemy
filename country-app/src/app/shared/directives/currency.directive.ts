import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: 'input[appCurrency]',
  standalone: true,
})
export class CurrencyDirective {
  // @Input()
  // set appCurrencyFormat(value: string) {
  //   if (value) {
  //     this.formatToCurrency(value);
  //   }
  // }

  // constructor(
  //   private el: ElementRef,
  //   private control: NgControl,
  // ) {}

  // @HostListener('blur', ['$event.target.value'])
  // onBlur(value: string) {
  //   this.formatToCurrency(value);
  // }

  // @HostListener('focus', ['$event.target.value'])
  // onFocus(value: string) {
  //   this.removeCurrencyFormat(value);
  // }

  // private formatToCurrency(value: string) {
  //   let val = value.replace(/,/g, '');
  //   if (!isNaN(Number(val))) {
  //     let formattedValue = new Intl.NumberFormat('de-DE', {
  //       style: 'currency',
  //       currency: 'EUR',
  //     }).format(parseFloat(val));
  //     this.el.nativeElement.value = formattedValue;
  //     this.control.control?.setValue(val);
  //   }
  // }

  // private removeCurrencyFormat(value: string) {
  //   let val = value.replace(/[€,.]/g, '');
  //   this.el.nativeElement.value = val;
  // }

  // ---------------2
  private locale: string = 'de-DE';
  private currency: string = 'EUR';

  constructor(
    private el: ElementRef<HTMLInputElement>,
    private renderer: Renderer2,
    private control: NgControl,
  ) {}

  // @HostListener('input', ['$event.target.value'])
  // onInput(value: string): void {
  //   let numericValue = this.parseValue(value);
  //   let formattedValue = this.formatToCurrency(numericValue);

  //   this.renderer.setProperty(this.el.nativeElement, 'value', formattedValue);
  //   this.control.control?.setValue(numericValue, { emitEvent: false });
  // }

  @HostListener('focus', ['$event.target.value'])
  onFocus(value: string): void {
    let clean = this.cleanFormat(value);
    let numericValue = this.parseValue(clean);

    console.log({ value, numericValue, clean });
    this.renderer.setProperty(this.el.nativeElement, 'value', numericValue);
  }

  @HostListener('blur', ['$event.target.value'])
  onBlur(value: string): void {
    let numericValue = this.parseValue(value);
    let formattedValue = this.formatToCurrency(numericValue);
    console.log({ value, numericValue, formattedValue });

    this.renderer.setProperty(this.el.nativeElement, 'value', formattedValue);
  }

  private parseValue(value: string): number {
    return parseFloat(value.replace(/[^0-9.-]+/g, '')) || 0;
  }

  private formatToCurrency(value: number): string {
    this.control.control?.setValue(this.roundedCurre(String(value)));
    return new Intl.NumberFormat(this.locale, {
      style: 'currency',
      currency: this.currency,
    }).format(value);
  }

  private cleanFormat(value: string) {
    return value
      .replace(/[^\d,-]/g, '')
      .replace('.', '')
      .replace(',', '.');
  }

  private roundedCurre(value: string): number {
    return this.parseValue(parseFloat(value).toFixed(2));
  }

  // ----------------------
  // @Input('appCurrencyFormat')
  // currencyCode: string = 'EUR';

  // @Input()
  // locale: string = 'de-DE';

  // private el: HTMLInputElement;

  // constructor(
  //   private elementRef: ElementRef,
  //   private renderer: Renderer2,
  // ) {
  //   this.el = this.elementRef.nativeElement;
  // }

  // @HostListener('input', ['$event.target.value'])
  // onInput(value: string): void {
  //   const numericValue = this.parseCurrency(value);
  //   this.renderer.setProperty(this.el, 'value', this.formatCurrency(numericValue));
  // }

  // @HostListener('blur')
  // onBlur(): void {
  //   const numericValue = this.parseCurrency(this.el.value);
  //   this.renderer.setProperty(this.el, 'value', this.formatCurrency(numericValue));
  // }

  // private formatCurrency(value: number): string {
  //   if (isNaN(value)) {
  //     return '';
  //   }

  //   return new Intl.NumberFormat(this.locale, {
  //     style: 'currency',
  //     currency: this.currencyCode,
  //     minimumFractionDigits: 2,
  //     maximumFractionDigits: 2,
  //   }).format(value);
  // }

  // private parseCurrency(value: string): number {
  //   const numberValue = value
  //     .replace(/[^\d,-]/g, '') // Remove non-numeric characters except for '-' and ','
  //     .replace(',', '.'); // Replace comma with dot for decimal conversion

  //   return parseFloat(numberValue) || 0;
  // }
}
