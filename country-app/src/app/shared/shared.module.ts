import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageComponent } from './pages/home-page/home-page.component';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';
import { MaterialModule } from '../material/material.module';
import { SearchTimerComponent } from './components/search-timer/search-timer.component';
import { SearchBoxTimerPageComponent } from './pages/search-box-timer-page/search-box-timer-page.component';
import { FormsModule } from '@angular/forms';
import { FormatCurrencyComponent } from './components/format-currency/format-currency.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MaterialModule,
    FormatCurrencyComponent,
  ],
  declarations: [
    HomePageComponent,
    AboutPageComponent,
    SidebarComponent,
    ContactPageComponent,
    SearchBoxComponent,
    SearchBoxTimerPageComponent,
    SearchTimerComponent,
  ],
  exports: [
    HomePageComponent,
    AboutPageComponent,
    SidebarComponent,
    ContactPageComponent,
    SearchBoxComponent,
    SearchBoxTimerPageComponent,
    SearchTimerComponent,
  ],
})
export class SharedModule {}
