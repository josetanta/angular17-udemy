import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'shared-search-box',
  templateUrl: './search-box.component.html',
  styles: ``,
})
export class SearchBoxComponent {
  @Input()
  public placeholder = '';

  @ViewChild('txtSearchInput')
  private searchRef!: ElementRef<HTMLInputElement>;

  @Output()
  public onValue = new EventEmitter<string>();

  handleKeyUpEnter() {
    const value = this.searchRef.nativeElement.value;

    // Emitt event
    this.onValue.emit(value);

    this.searchRef.nativeElement.value = '';
  }
}
