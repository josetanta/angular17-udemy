import { Component, Input, OnInit } from '@angular/core';
import { PersonFilter } from '../../models/person-filter';
import { asyncScheduler, debounceTime, Subject } from 'rxjs';

@Component({
  selector: 'app-search-timer',
  templateUrl: './search-timer.component.html',
  styleUrl: './search-timer.component.scss',
})
export class SearchTimerComponent implements OnInit {
  @Input({
    required: true,
  })
  filtering: PersonFilter;
  filteringChanged = new Subject<PersonFilter>();

  disabled: boolean = false;

  dataPerson = [...Array(10).keys()].map((k) =>
    PersonFilter.createPersonFilter(
      `Firstname-${k + 1}`,
      `Lastname-${k + 1}`,
      new Date(),
    ),
  );

  constructor() {
    this.filteringChanged.pipe(debounceTime(1500, asyncScheduler)).subscribe((model) => {
      console.log('----- Filtering -----', model);
      this.disabled = true;
      const timeout = setTimeout(() => {
        this.disabled = false;
        console.log('Waiting for results from API');
        clearTimeout(timeout);
      }, 1000);
    });
  }

  ngOnInit(): void {}

  handleFieldChange(obj: any) {
    this.filteringChanged.next({ ...this.filtering, ...obj });
  }

  handleRestFormClick() {
    this.filtering = new PersonFilter();
    this.filteringChanged.next(this.filtering);
  }
}
