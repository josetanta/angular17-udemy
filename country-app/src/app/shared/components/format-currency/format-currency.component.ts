import { CommonModule, CurrencyPipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CurrencyDirective } from '../../directives/currency.directive';

@Component({
  selector: 'app-format-currency',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    CurrencyDirective,
  ],
  templateUrl: './format-currency.component.html',
  styleUrl: './format-currency.component.scss',
  providers: [CurrencyPipe],
})
export class FormatCurrencyComponent implements OnInit {
  protected formGroup = this.fb.group({
    currency: ['', [Validators.required]],
  });

  constructor(
    private fb: FormBuilder,
    private curr: CurrencyPipe,
  ) {}

  ngOnInit(): void {}

  // formatCurrency(value: number | string | null): string {
  //   if (value === null) return '';

  //   const formattedValue = new Intl.NumberFormat('de-DE', {
  //     style: 'currency',
  //     currency: 'EUR',
  //     minimumFractionDigits: 2,
  //   }).format(typeof value === 'string' ? parseFloat(value) : value);

  //   return formattedValue;
  // }
  // parseCurrency(value: string): number {
  //   const numberValue = value.replace(/[^\d,-]/g, '').replace(',', '.');
  //   return parseFloat(numberValue);
  // }
}
