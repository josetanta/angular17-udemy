import { Component } from '@angular/core';
import { CountriesService } from '../../services/countries.service';
import { Country } from '../../types/search.reponse';

@Component({
  selector: 'countries-by-capital-page',
  templateUrl: './by-capital-page.component.html',
  styles: ``,
})
export class ByCapitalPageComponent {
  public countries: Array<Country> = [];
  constructor(private countryService: CountriesService) {}

  handleByCapital(value: string) {
    console.log({ value });
    this.countryService.searchBycapital(value).subscribe((countries) => {
      this.countries = countries;
    });
  }
}
