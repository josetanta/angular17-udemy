import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from '../types/search.reponse';
import { catchError, of, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CountriesService {
  private readonly HOST = 'https://restcountries.com/v3.1';
  constructor(private http: HttpClient) {}

  searchBycapital(capital: string) {
    return this.http.get<Array<Country>>(`${this.HOST}/capital/${capital}`).pipe(
      tap((countries) => console.log(`Paso por el tap`, countries)),
      catchError((err) => of([])),
    );
  }
}
